<?php
use Phalcon\Http\Response;

$app->post(
	'/greatreset',
	function () use ($app)
	{
		$bot = $app->request->getJsonRawBody();
		$var = $app->getSharedService('db');

		$statement = $var->prepare('CALL GREATRESET()');
		$success = $statement->execute();


		$response = new Response();

		if($success) {
			$response->setStatusCode(201, 'Reset');
			$response->setJsonContent(
				[
					'status'	=>	'OK',
					'data'		=>	$bot,
				]
			);
		} else {
			$response->setStatusCode(409, 'Conflict');

			$errors = [];
			foreach ($status->getMessages() as $message) {
				$errors[] = $message->getMessage();
			}
			$response->setJsonContent(
				[
					'status'	=> 'ERROR',
					'messages'	=> $errors,
				]
			);
		}
		return $response;
	}

);
?>
