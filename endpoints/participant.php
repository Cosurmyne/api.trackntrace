<?php
use Phalcon\Http\Response;
/**
	* Student identifies themself
 */
$app->get(
	'/participant/{id:[0-9]+}',
	function ($id) use ($app)
	{
		$phql = 'SELECT * FROM Root\Models\Participant WHERE STUDENT_NO = :id:';
		$participant = $app->modelsManager->executeQuery($phql,['id' => $id])->getFirst();
		$response = new Response();

		if($participant === false) $response->setJsonContent(['status' => 'NOT-FOUND']);
		else $response->setJsonContent(
			[
				'status'		=> 'FOUND',
				'data'			=> [
					'id'		=> $participant->ID,
					'fname'		=> $participant->FIRST_NAME,
					'lname'		=> $participant->LAST_NAME,
					'title'		=> $participant->TITLE,
					'consent'	=> $participant->CONSENT
				]
			]
		);
		return $response;
	}

);

/**
	* Student Signs Consent Form
 */
$app->put(
	'/participant/consent/{id}',
	function ($id) use ($app)
	{
		$participant	= $app->request->getJsonRawBody();
		$phql		= 'UPDATE Root\Models\Participant SET CONSENT = NOW() WHERE ID = :id:';
		$status		= $app->modelsManager->executeQuery( $phql, ['id' => $id ] );
	}
);
?>
