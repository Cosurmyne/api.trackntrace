<?php

$app->get(
	'/questions',
	function () use ($app) {
		$phql = 'SELECT ID, TYPE FROM Root\Models\Question ORDER BY TYPE';
		$questions = $app->modelsManager->executeQuery($phql);
		$data = [];
		foreach ($questions as $quest) {
			$data[] = [
				'id'   => $quest->ID,
				'name' => $quest->TYPE,
			];
		}
		return json_encode($data);
	}
);

$app->get('/synopsis',
	function () use ($app)
	{
		$phql	= 'SELECT ID FROM Root\Models\Synopsis';
		$ids	= $app->modelsManager->executeQuery($phql);
		return json_encode($ids);
	}
	
);

$app->get('/synopsis/{id:[0-9]+}',
	function ($id) use ($app)
	{
		$phql	= 'SELECT TALE FROM Root\Models\Synopsis WHERE ID = :id:';
		$ids	= $app->modelsManager->executeQuery($phql,['id' => $id])->getFirst();
		return json_encode($ids);
	}
	
);

?>
