<?php
namespace Root\Models;

use Phalcon\Mvc\Model;

class Synopsis extends Model
{
	public $id;
	public $tale;

	/**
	* undocumented function
	*
	* @return void
	*/
	public function initialize()
	{
		$this->setSource('synopsis');
		$this->hasMany(
			'id',
			Question::class,
			'synopsis'
		);
	}
	
}

?>
