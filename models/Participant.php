<?php
namespace Root\Models;

use Phalcon\Mvc\Model;

class Participant extends Model
{
	public $id;
	public $title;
	public $first_name;
	public $last_name;
	public $student_no;
	public $consent;

	/**
	* undocumented function
	*
	* @return void
	*/
	public function initialize()
	{
		$this->setSource('participant');
	}
	
}
?>
