<?php
namespace Root\Models;

use Phalcon\Mvc\Model;

class Question extends Model
{
	public $id;
	public $question;
	public $type;
	public $objective;
	public $synopsis;

	/**
	* undocumented function
	*
	* @return void
	*/
	public function initialize()
	{
		$this->setSource('question');
		$this->belongsTo(
			'synopsis',
			Synopsis::class,
			'id'
		);
	}
	
}
?>
