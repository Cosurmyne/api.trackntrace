<?php

use Phalcon\Loader;
use Phalcon\Mvc\Micro;
use Phalcon\Di\FactoryDefault;
use Phalcon\Db\Adapter\Pdo\Mysql as PdoMysql;


$loader = new Loader();
$loader->registerNamespaces(
	[
		'Root\Models' => __DIR__ . '/models/',
	]
);
$loader->register();

$container = new FactoryDefault();
$container->set(
	'db',
	function () {
		return new PdoMysql(
			[
				'host'	=> '127.0.0.1',
				'username' => 'nmu',
				'dbname' => 'trackntrace',
			]
		);
	}
);

$app = new Micro($container);

require_once 'endpoints/global.php';
require_once 'endpoints/participant.php';
require_once 'endpoints/question.php';

$app->handle(
	$_SERVER["REQUEST_URI"]
);
