-- MySQL dump 10.17  Distrib 10.3.25-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: trackntrace
-- ------------------------------------------------------
-- Server version	10.3.25-MariaDB-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `trackntrace`
--

/*!40000 DROP DATABASE IF EXISTS `trackntrace`*/;

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `trackntrace` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `trackntrace`;

--
-- Table structure for table `answer`
--

DROP TABLE IF EXISTS `answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answer` (
  `ID` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `ANSWER` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `objective`
--

DROP TABLE IF EXISTS `objective`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `objective` (
  `ID` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `DESCRIPTION` varchar(48) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `DESCRIPTION` (`DESCRIPTION`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `option`
--

DROP TABLE IF EXISTS `option`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `option` (
  `QUESTION` tinyint(3) unsigned NOT NULL,
  `ANSWER` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`QUESTION`,`ANSWER`),
  KEY `ANSWER` (`ANSWER`),
  CONSTRAINT `option_ibfk_1` FOREIGN KEY (`QUESTION`) REFERENCES `question` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `option_ibfk_2` FOREIGN KEY (`ANSWER`) REFERENCES `answer` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `participant`
--

DROP TABLE IF EXISTS `participant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `participant` (
  `ID` char(3) CHARACTER SET utf8 NOT NULL,
  `FIRST_NAME` varchar(20) NOT NULL,
  `LAST_NAME` varchar(20) NOT NULL,
  `STUDENT_NO` char(9) CHARACTER SET utf8 DEFAULT NULL,
  `TITLE` enum('Mr','Miss','Prof','Dr') DEFAULT NULL,
  `CONSENT` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `STUDENT_MO` (`STUDENT_NO`),
  UNIQUE KEY `STUDENT_NO` (`STUDENT_NO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `qtype`
--

DROP TABLE IF EXISTS `qtype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qtype` (
  `ID` char(2) CHARACTER SET utf8 NOT NULL,
  `NAME` varchar(20) NOT NULL,
  `DESCRIPTION` varchar(95) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `NAME` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question` (
  `ID` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `QUESTION` varchar(255) NOT NULL,
  `TYPE` char(2) CHARACTER SET utf8 NOT NULL,
  `OBJECTIVE` tinyint(3) unsigned NOT NULL,
  `SYNOPSIS` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `QUESTION` (`QUESTION`),
  KEY `TYPE` (`TYPE`),
  KEY `OBJECTIVE` (`OBJECTIVE`),
  KEY `SYNOPSIS` (`SYNOPSIS`),
  CONSTRAINT `question_ibfk_1` FOREIGN KEY (`TYPE`) REFERENCES `qtype` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `question_ibfk_2` FOREIGN KEY (`OBJECTIVE`) REFERENCES `objective` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `question_ibfk_3` FOREIGN KEY (`SYNOPSIS`) REFERENCES `synopsis` (`ID`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `response`
--

DROP TABLE IF EXISTS `response`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `response` (
  `PARTICIPANT` char(3) CHARACTER SET utf8 NOT NULL,
  `QUESTION` tinyint(3) unsigned NOT NULL,
  `MODIFIED` datetime DEFAULT current_timestamp(),
  PRIMARY KEY (`PARTICIPANT`,`QUESTION`),
  KEY `QUESTION` (`QUESTION`),
  CONSTRAINT `response_ibfk_1` FOREIGN KEY (`PARTICIPANT`) REFERENCES `participant` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `response_ibfk_2` FOREIGN KEY (`QUESTION`) REFERENCES `question` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sheet`
--

DROP TABLE IF EXISTS `sheet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sheet` (
  `PARTICIPANT` char(3) CHARACTER SET utf8 NOT NULL,
  `QUESTION` tinyint(3) unsigned NOT NULL,
  `ANSWER` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`PARTICIPANT`,`QUESTION`,`ANSWER`),
  KEY `QUESTION` (`QUESTION`,`ANSWER`),
  CONSTRAINT `sheet_ibfk_1` FOREIGN KEY (`PARTICIPANT`, `QUESTION`) REFERENCES `response` (`PARTICIPANT`, `QUESTION`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sheet_ibfk_2` FOREIGN KEY (`QUESTION`, `ANSWER`) REFERENCES `option` (`QUESTION`, `ANSWER`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `synopsis`
--

DROP TABLE IF EXISTS `synopsis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `synopsis` (
  `ID` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `TALE` text NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-11-10 14:49:23
