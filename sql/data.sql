-- MySQL dump 10.17  Distrib 10.3.25-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: trackntrace
-- ------------------------------------------------------
-- Server version	10.3.25-MariaDB-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `answer`
--

LOCK TABLES `answer` WRITE;
/*!40000 ALTER TABLE `answer` DISABLE KEYS */;
INSERT INTO `answer` VALUES (1,'Your Name'),(2,'Your Address'),(3,'Full Postal Code'),(4,'Your Test Results'),(5,'Next of Kin'),(6,'Medical Aid Information'),(7,'**Without** the terms of a Statutory provision'),(8,'**Without** the instruction of a court'),(9,'**Outside** the public interest'),(10,'**Without** your consent expressed orally or in writing'),(11,'**Without** your written consent of a minor under your care'),(12,'I strongly agree'),(13,'I can tolerate'),(14,'I not certain'),(15,'I\'m uncomfortable with it'),(16,'I\'m against it'),(17,'Reluctant'),(18,'1'),(19,'2'),(20,'3'),(21,'4'),(22,'5'),(23,'Compliant'),(24,'Practitioners didn\'t seek your consent'),(25,'Practitioners didn\'t anonymize data where unidentifiable data will serve the purpose'),(26,'Yes'),(27,'No'),(28,'Uncertain'),(29,'Unethical'),(30,'1'),(31,'2'),(32,'3'),(33,'4'),(34,'5'),(35,'Ethical'),(36,'Breach of Contract'),(37,'Abuse of Power'),(38,'Not in the Public Interest'),(39,'Violation of Human Rights'),(40,'0'),(41,'1'),(42,'2'),(43,'3'),(44,'Health care practitioners have a duty to protect the privacy of patients and respect their autonomy'),(45,'The patient\'s right to refuse a health service'),(46,'Transparency with patient about the use of their data');
/*!40000 ALTER TABLE `answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `objective`
--

LOCK TABLES `objective` WRITE;
/*!40000 ALTER TABLE `objective` DISABLE KEYS */;
INSERT INTO `objective` VALUES (2,'Adverse elements of Contact Tracing'),(3,'Concerns about current Contact Tracing Methods'),(1,'Medical Privacy and Confidentiality');
/*!40000 ALTER TABLE `objective` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `option`
--

LOCK TABLES `option` WRITE;
/*!40000 ALTER TABLE `option` DISABLE KEYS */;
INSERT INTO `option` VALUES (1,1),(1,2),(1,3),(1,4),(1,5),(1,6),(2,7),(2,8),(2,9),(2,10),(2,11),(3,12),(3,13),(3,14),(3,15),(3,16),(4,17),(4,18),(4,19),(4,20),(4,21),(4,22),(4,23),(5,24),(5,25),(6,26),(6,27),(6,28),(7,29),(7,30),(7,31),(7,32),(7,33),(7,34),(7,35),(8,36),(8,37),(8,38),(8,39),(9,40),(9,41),(9,42),(9,43),(10,40),(10,41),(10,42),(10,43),(11,40),(11,41),(11,42),(11,43),(12,44),(12,45),(12,46);
/*!40000 ALTER TABLE `option` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `participant`
--

LOCK TABLES `participant` WRITE;
/*!40000 ALTER TABLE `participant` DISABLE KEYS */;
INSERT INTO `participant` VALUES ('AA1','Aphiwe','Adonisi','216435773','Mr',NULL),('AN1','Nadia','Azem','20604612','Miss',NULL),('BC1','Craig','Burjins','216308585','Mr',NULL),('BN1','Nicholas','Bouwer','214347427','Mr',NULL),('BS1','Sive','Buhe','215115961','Miss',NULL),('BZ1','Zenande','Bekebu','214056791','Miss',NULL),('CK1','Khanyisa','Cakwe','214129195','Mr',NULL),('CS1','Sarah','Chivers','216310962','Miss',NULL),('DJ1','Joel','Darier','215164792','Mr',NULL),('DN1','Nonophela','Dlembula','216093309','Mr',NULL),('DR1','Rushdeen','Davis','216252261','Mr',NULL),('DS1','Sibusiso','Duma','216369274','Mr',NULL),('DV1','Vuyani','Daniso','213501635','Mr',NULL),('GC1','Chwayita','Gwantshu','214334503','Miss',NULL),('GC2','Clyde','Galiant','213332590','Mr',NULL),('GC3','Chuma','Gantsho','214133176','Mr',NULL),('GD1','Donald','Goromondo','211142018','Mr',NULL),('GL1','Lukhona','Gwegwe','216658977','Miss',NULL),('GS1','Siphosethu','Gcobo','215158040','Mr',NULL),('GS2','Siyasanga','Gcilishe','215336372','Mr',NULL),('HJ1','Justin','Hein','215109465','Mr',NULL),('HL1','Luke','Hamilton','216005299','Mr',NULL),('JC1','Cinchona','Janse Van Rensburg','213295679','Mr',NULL),('JS1','Sive','Jilili','215292405','Mr',NULL),('KK1','Keanu','Kisten','214166708','Mr',NULL),('LA1','Andisiwe','Lukwe','213477947','Miss',NULL),('LN1','Nhlanhla','Lupuwana','215285174','Mr',NULL),('LS1','Siphumze','Labiti','216100801','Mr',NULL),('MA1','Axola','Mgudlandlu','213471434','Mr',NULL),('MA2','Asekhona','Makisi','213311860','Mr',NULL),('MA3','Alungile','Mkosi','215184432','Miss',NULL),('MC1','Chulumanco','Mtshizana','215132750','Mr',NULL),('MD1','Dalekile','Modikwe','215032861','Mr',NULL),('MD2','Daluxolo','Msikinya','215013395','Mr',NULL),('ME1','Ezile','Majikijela','215087836','Mr',NULL),('MK1','Kholosa','Mdliva','215197976','Mr',NULL),('MK2','Khaya','Mphetshethwa','215231341','Mr',NULL),('ML1','Lunga','Mbangata','214076091','Mr',NULL),('ML2','Lelethu','Maku','214338940','Mr',NULL),('MM1','Mukhethwa','Masethe','220267642','Miss',NULL),('MN1','Ntsikayomzi','Maqetuka','215359275','Mr',NULL),('MN2','Nontuthuzelo','Masinyane','214345033','Miss',NULL),('MN3','Nikitha','Mathangana','215177770','Mr',NULL),('MS1','Sibusiso','Mabasa','214185923','Mr',NULL),('MS2','Samkele','Moloi','216102375','Mr',NULL),('MS3','Simphiwe','Madubela','214354717','Mr',NULL),('MS4','Suku','Mndela','213241897','Mr',NULL),('MT1','Tebogo','Malelfane','214092429','Mr',NULL),('MV1','Vuyisa','Mbokotho','216058244','Mr',NULL),('MY1','Yonela','Maxhama','213502127','Miss',NULL),('NB1','Busiswa','Nqala','215307445','Miss',NULL),('NK1','Keith','Nukeri','215158415','Mr',NULL),('NL1','Lulutho','Ndevu','214112020','Mr',NULL),('NM1','Michael','Ndowora','216866014','Mr',NULL),('NN1','Ntombizandile','Nkayi','215120477','Miss',NULL),('NN2','Ntuthuko','Ndlovu','216008581','Mr',NULL),('NT1','Thandolwethu','Ntshintshi','213226170','Mr',NULL),('NZ1','Zintle','Njombolwana','214046516','Miss',NULL),('OD1','David','Oliphant','214298841','Mr',NULL),('OP1','Paa','Owusu-Ansah','215085337','Mr',NULL),('PA1','Abre','Pio','215333608','Mr',NULL),('RL1','Luxolo','Ralawe','212406361','Mr',NULL),('RM1','Mawethu','Rubuxa','215099834','Mr',NULL),('SL1','Lindokuhle','Sonto','214054640','Mr',NULL),('SL2','Luyolo','Satyo','215106407','Mr',NULL),('SL3','Luxolo','Sonto','213235919','Mr',NULL),('SR1','Roan','Swanepoel','219019681','Mr',NULL),('SS1','Shadrack','Sithole','214005658','Mr',NULL),('TB1','Baphiwe','Tukela','215083806','Mr',NULL),('TE1','Entle','Tutu','216731526','Miss',NULL),('TM1','Michael','Thomas','215108590','Mr',NULL),('TX1','Xolelwa','Tshingana','215165462','Miss',NULL),('VE1','Ethan','Van Der Mescht','215005899','Mr',NULL),('WN1','Nathan','Waters','215250567','Mr',NULL),('XC1','Chwayita','Xengxe','214112330','Miss',NULL),('XS1','Sive','Xakwe','215167635','Miss',NULL),('YA1','Ailan','Yon','213502399','Mr',NULL),('ZT1','Thanduxolo','Zonke','216988942','Mr',NULL);
/*!40000 ALTER TABLE `participant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `qtype`
--

LOCK TABLES `qtype` WRITE;
/*!40000 ALTER TABLE `qtype` DISABLE KEYS */;
INSERT INTO `qtype` VALUES ('CB','Multi selection','Multiple Choice : Many Options Possible'),('LP','LPC Scale','Respondent has to indicate a preference between two explicit alternatives'),('LS','Lickert Scale','Specify degree to which statement applies to respondent'),('RB','Single Alternative','Multiple Choice : One Option Possible');
/*!40000 ALTER TABLE `qtype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `question`
--

LOCK TABLES `question` WRITE;
/*!40000 ALTER TABLE `question` DISABLE KEYS */;
INSERT INTO `question` VALUES (1,'Select _any number_ of boxes in the following, to indicate the information you do not want **publicly** disclosed of yourself.','CB',1,1),(2,'Select _any number_ of boxes in the following, to indicate conditions you consider to be a violation of privacy when information is **divulged** to a third party.','CB',1,2),(3,'Do you agree with the disclosure of information of the patient or other persons, where they would **not** be prone to harm as a result of risk related contact?','RB',1,3),(4,'Without assurances of confidentiality, indicate how likely you are to be **reluctant** / **compliant** to give practitioners the information they want in order to provide medical care,  with the scale between **1** and **5**. Mark the relevant box.','LP',1,4),(5,'Where health care practitioners are asked to provide information about patients, do you consider it a violation if:','CB',1,5),(6,'Do you consider it a violation if practitioners don’t disclose to you the full content of what they’ll do with information they collect from you?','RB',1,6),(7,'With a scale between **1** and **5** indicate the extent to which you consider the establishment the COVID-19 Tracing Database to keep personal information of infected/suspected persons, to be ethical.','LP',2,7),(8,'Select _any number_ of boxes in the following, to indicate what you consider true about the above described amendment.','CB',2,8),(9,'With the scale from **0** to **3**, indicate how likely you are to install the contact tracers app in your own device, after taking into consideration the above. (0 = never, 1 = least likely, 3 = very likely)','LS',2,9),(10,'With the scale from **0** to **3**, indicate how comfortable you are of the fact that contact tracing is done by a Non-Governmental Organization. (0 = uncomfortable, 3 = very comfortable)','LS',3,10),(11,'With the scale from **0** to **3**, indicate how likely you are to comply with contact tracers over the phone. (0 = never, 1 = least likely, 3 = very likely)','LS',3,11),(12,'Select _any number_ of boxes in the following, to indicate the provisions by the National Health Act being subverted by the described above.','CB',3,12);
/*!40000 ALTER TABLE `question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `response`
--

LOCK TABLES `response` WRITE;
/*!40000 ALTER TABLE `response` DISABLE KEYS */;
/*!40000 ALTER TABLE `response` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `sheet`
--

LOCK TABLES `sheet` WRITE;
/*!40000 ALTER TABLE `sheet` DISABLE KEYS */;
/*!40000 ALTER TABLE `sheet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `synopsis`
--

LOCK TABLES `synopsis` WRITE;
/*!40000 ALTER TABLE `synopsis` DISABLE KEYS */;
INSERT INTO `synopsis` VALUES (1,'**Anonymous Data**. Is defined by the _HEALTH PROFESSIONS COUNCIL OF SOUTH AFRICA_, to mean data from which the patient cannot be identified by the recipient of the information.'),(2,'**Statutory Provision**. means a provision, whether of a general or a special nature, contained in, or in any document made or issued under, any Act, whether of a general or special nature.'),(3,'On June 8, 2020, **Maria Van Kerkhove**, the _World Health Organization\'s_ technical lead for the COVID-19 response, said it\'s \"very rare\" for asymptomatic carriers of COVID-19 to spread the virus.'),(4,'The National Health Act (Act No. 61 of 2003) states that all patients have a right to confidentiality and this is consistent with the right to privacy in the South African Constitution (Act No. 108 of 1996)'),(5,'Chapter 3 of the Amendment to the Disaster Management Act Regulations - introduces the establishment of a COVID-19 **Tracing Database**, in which personal information about an infected person or a person suspected to be infected is kept.'),(6,'It is particularly important to check that patients understand what will be disclosed if it is necessary to share personal information with anyone employed by another organization or agency providing health or social care.'),(7,'The **National Health Act** requires that health care providers and health care establishments are responsible for personal information about their patients and must make sure that such information is effectively protected against improper disclosure at all times.'),(8,'In terms of the Amendment to the Disaster Management Act Regulations, people who have been tested for COVID-19 do **not need to be notified** or to provide their consent for a Mobile Network Operator to share their location or movements with the Director General of the Department of Health.'),(9,'British newspaper, The Sun, reports in the September 28, 2020 article that “**POLICE been told not to download the NHS Covid-19 track and trace app** - despite officers visiting the homes of self-isolating Brits under a new coronavirus crackdown”.'),(10,'To support the Department of Public Health (DPH), _Partners In Health_ (PIH) will hire, train and supervise a large team of community contact tracing staff across Massachusetts.'),(11,'The job is done exclusively by phone and the workers are trained in handling what could be difficult conversations.'),(12,'Urban Infrastructure Minister **Alan Tudge** has revealed Australia\'s incoming passenger card system will be digitized to assist with contact tracing for future international arrivals.<br/> \"From a contact-tracing perspective, we\'ll be able to immediately have the **information connected to contact-tracing** capability in each state and territory jurisdiction should they need it.\".');
/*!40000 ALTER TABLE `synopsis` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-11-10 14:50:13
